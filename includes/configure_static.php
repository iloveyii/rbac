<?php

/**
 * Set database details here
 */
define('DBHOST', 'localhost');
define('DBNAME', 'rbac');
define('DBUSER', 'root');
define('DBPASS', 'root');

/**
 * Set file locations here
 */
define('BAS_DIR', '/rbac');
define('JS_DIR', BAS_DIR . '/js/');
define('CSS_DIR', BAS_DIR . '/css/');
define('IMG_DIR', realpath(dirname(__FILE__).'/../images/').'/');

/*
 * Mapping labels to file names
 */
$arJsFilesNames = [
    'jquery'=>'jquery.js',
    'bootstrap'=>'bootstrap.js',
    'chat'=>'chat.js',
];

$arCssFilesNames = [
    'bootstrap'=>'bootstrap.css',
];

