<?php

 class User 
 {
        private $userRoles = array();
        private static $db;

        /* Connects to database */
        private function connectDB(){
                return new PDO(
                    "mysql:host=".DBHOST.";dbname=".DBNAME.";charset=utf8",DBUSER,DBPASS
                );
        }
 	
 	//Populate the user object when it's created
 	public function __construct($user_id) {
            self::$db = $this->connectDB();
            
            $getUser = self::$db->prepare("SELECT * FROM user WHERE user_id = :userid ");
            $getUser->execute(array(":userid" => $user_id));
            if($getUser->rowCount() == 1)
            {
                    $userData = $getUser->fetch(PDO::FETCH_ASSOC);
                    $this->user_id = $user_id;
                    $this->username = ucfirst($userData['username']);
                    $this->email = $userData['email_address'];
                    //etc.. More data if needed
                    $this->loadRoles();//Initiate the userroles
            }
 	}
 	
	//Fill the array with this user's roles, it's
        protected function loadRoles()
        {
            $fetchRoles = self::$db->prepare("SELECT user_role.role_id, role.role_name FROM user_role JOIN role ON user_role.role_id = role.role_id WHERE user_role.user_id = :user_id");
            $fetchRoles->execute(array(":user_id" => $this->user_id));

                    //Populate the array
            while($row = $fetchRoles->fetch(PDO::FETCH_ASSOC))
            {
                $this->userRoles[$row["role_name"]] = Role::getRole($row["role_id"]);
            }
            var_dump($this->userRoles);
        }

        //Check if the user has a certain permission
        public function hasPermission($permission)
        {
            //If the user has more roles, check them too
            foreach ($this->userRoles as $role)
            {
                    //Do the actual checking
                if ($role->verifyPermission($permission))
                {
                    return TRUE;
                }
            }
            return FALSE;
        }
}