<!DOCTYPE html>
<?php
/**
 * Include the necessary files
 */
require 'includes/configure_static.php';
require 'includes/functions.php';
?>

<?php 
    // Register autoload
    function __autoload($class_name) {
        $filename = strtolower($class_name).'.php';
        if(file_exists('includes/'. $filename)) {
            require_once 'includes/'. $filename;
            return;
        } elseif (file_exists('doaction/'. $filename)) {
            require_once 'doaction/'. $filename;
            return;
        }
        die($filename);
    }

?>

<?php 

    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    /**
     * Add the common js and css files
     */
    AddScript::addJS('jquery');
    AddScript::addJS('bootstrap');

    AddScript::addCSS('bootstrap');
?>

<?php 

    /**
     * The router is here
     */
     $action = getValue('action');
     
     // $user = new User($_SESSION["user_id"]);
     $user = new User(1);
     
     /**
     * Get the page in strPage
     */
     switch ($action) {
        case 'chat':
            if($user->hasPermission('chat')) {
                $page = new Chat();
                $page->body();
            } else {
                $strPage = Html::alert('You have no permissions to view this page', TRUE);
            }
            
            break;
        
        case 'fileupload':
            if($user->hasPermission('fileupload')) {
                // another page
                $strPage = Html::h(3, 'This is just another page for which you got persmissons.');
            } else {
                $strPage = Html::alert('You have no permissions to view this page', TRUE);
            }
            break;
        
        default:
            $page = new Chat();
            $page->body();
            break;
    }
     
    /**
     * Render the view
     */
    if(isset($page)) {
        $page->execute();
        $strPage = $page->renderBody();
    }
     

?>
<html>
    <!-- Add head section, this includes all js and css files -->
    <?php require 'parts/head.php'; ?>
    
    <!-- Add start of body section -->
    <?php require 'parts/startBody.php'; ?>
    
        <a class="btn btn-success" href="chat">Start Chat</a>
        <a class="btn btn-success" href="fileupload">File Upload</a>
        <br /><br />
        <?php echo $strPage; ?>
        
    <!-- Add end of body section -->
    <?php require 'parts/endBody.php'; ?>
</html>

